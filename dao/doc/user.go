package doc

import (
	"apiscerana/core/dao"
	"crypto/md5"

	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	userC      = "user"
	userTokenC = "userToken"
)

type SimpleUser struct {
	ID      bson.ObjectId
	Account string
	Name    string
}

type ProjectFolder struct {
	Name        string   `json:"name"`
	ProjectList []string `json:"projectList"`
}

type User struct {
	ID            primitive.ObjectID  `json:"ID,omitempty" bson:"_id"`
	Account       string              `json:"account,omitempty"`
	Email         string              `json:"email,omitempty"`
	Description   string              `json:"description,omitempty"`
	Pwd           string              `json:"pwd,omitempty"`
	Sys           []userSys           `json:"-"`
	ParentUser    *primitive.ObjectID `json:"-"`
	State         string              `json:"-"`
	Enable        bool                `json:"-"`
	MustChangePwd bool                `json:"-"`
	SubUsers      []User              `json:"SubUsers,omitempty" bson:"-"`
	Group         string              `json:"group,omitempty" bson:"-"`
	Systems       []string            `json:"system,omitempty" bson:"-"`
	Company       string              `json:"Company,omitempty"`
	Name          string              `json:"name"`
	Projects      []Project           `json:"-" bson:"lookupProjects,omitempty"`
	HotEmailList  []string            `json:"-"`

	ProjectFolderList []ProjectFolder `json:"-"`
	AppConnNumber     uint8           `json:"-"`
	DeskConnNumber    uint8           `json:"-"`
	//Email   string `json:"Email,omitempty"`
	//	Phone         string         `json:"Phone,omitempty"`
	//	Address       string         `json:"Address,omitempty"`
	//	Telephone     string         `json:"Telephone,omitempty"`
	dao.CommonDoc `bson:"meta"`
}

func (u *User) GetDoc() interface{} {
	return u
}

func (u *User) GetC() string {
	return userC
}

func (u *User) GetID() primitive.ObjectID {
	return u.ID
}

func (u *User) GetIndexes() []mongo.IndexModel {
	return nil
}

func (u *User) GetAccount() string {
	return u.Account
}

func (u *User) GetName() string {
	return u.Name
}

func (u *User) GetGroup(sysCode string) string {
	for _, val := range u.Sys {
		if val.SysCode == sysCode && val.Status {
			return val.Group
		}
	}
	return ""
}

func (u *User) AddSys(syscode, group string) {
	u.Sys = append(u.Sys, userSys{
		SysCode: syscode,
		Group:   group,
		Status:  true,
	})
	return
}

type userSys struct {
	SysCode    string `json:"SysCode,omitempty"`
	Group      string `json:"Group,omitempty"` // 紀錄權限
	Status     bool   `json:"Status,omitempty"`
	CreateTime int64  `json:"CreateTime,omitempty"`
	UpdateTime int64  `json:"UpdateTime,omitempty"`
}

type UserPushToken struct {
	ID            primitive.ObjectID `bson:"_id"`
	PushToken     string
	Account       string
	Sys           string
	Platform      string
	PhoneID       string
	dao.CommonDoc `bson:"meta"`
}

func (u *UserPushToken) newID() primitive.ObjectID {
	var b [12]byte
	id := b[:]
	hw := md5.New()
	hw.Write([]byte(u.PushToken))
	copy(id, hw.Sum(nil))
	return b
}

func (u *UserPushToken) GetDoc() interface{} {
	if u.ID.IsZero() {
		u.ID = u.newID()
	}
	return u
}

func (u *UserPushToken) GetC() string {
	return userTokenC
}

func (u *UserPushToken) GetID() primitive.ObjectID {
	return u.ID
}

func (u *UserPushToken) GetIndexes() []mongo.IndexModel {
	return nil
}
