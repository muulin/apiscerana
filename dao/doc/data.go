package doc

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"fmt"
	"strconv"
	"strings"

	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"apiscerana/util"
)

const (
	dataC           = "data"
	DataQueueNumber = 10
)

type Data map[string]interface{}

func (d Data) GetC() string {
	return d.GetID().Timestamp().Format(dataC + "-20060102")
}

func (d Data) newID() (primitive.ObjectID, error) {
	var b [12]byte
	// Timestamp, 4 bytes, big endian
	timestamp, ok := d["Timestamp_Unix"].(float64)
	if !ok {
		return b, fmt.Errorf("Filed Timestamp_Unix error: %v", d["Timestamp_Unix"])
	}
	binary.BigEndian.PutUint32(b[:], uint32(timestamp))
	// boxmd5, first 6 bytes of md5(hostname)
	boxMD5 := GetBoxMD5(d["MAC_Address"].(string), d["GW_ID"].(string))
	boxId := []byte(boxMD5)
	b[4] = boxId[0]
	b[5] = boxId[1]
	b[6] = boxId[2]
	b[7] = boxId[3]
	b[8] = boxId[4]
	b[9] = boxId[5]
	// device & address for 2 bytes
	b[10] = byte(0)
	b[11] = byte(0)
	return b, nil
}

func (d Data) GetID() primitive.ObjectID {
	if id, ok := d["_id"]; ok {
		return id.([12]byte)
	}
	id, _ := d.newID()
	return id
}

func (d Data) GetDoc() interface{} {
	id := d.GetID()
	if id.IsZero() {
		return nil
	}
	d["_id"] = id
	d["Timestamp_Unix"] = uint32(d["Timestamp_Unix"].(float64))
	return d
}

func (d *Data) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bson.D{
				{Name: "GW_ID", Value: 1},
				{Name: "Timestamp_Unix", Value: -1},
				{Name: "MAC_Address", Value: 1},
			},
		},
	}
}

// func (d Data) UpdateField(mdb *mgo.Database, fields bson.M) error {
// 	return mdb.C(DataC).UpdateId(d["_id"], bson.M{"$set": fields})
// }

// func (d Data) BackUpByDate(mdb *mgo.Database) error {
// 	if mdb == nil {
// 		return errors.New("mdb is nil")
// 	}
// 	oid, err := d.getID()
// 	if err != nil {
// 		return err
// 	}
// 	backC := oid.Time().Format(DataC + "-20060102")
// 	_, err = mdb.C(backC).Upsert(bson.M{"_id": oid}, d)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// func (d Data) Backup(sourcedb *mgo.Database, targetdb *mgo.Database) error {
// 	oid, err := d.getID()
// 	if err != nil {
// 		return err
// 	}
// 	backC := oid.Time().Format(DataC + "-20060102")
// 	_, err = targetdb.C(backC).Upsert(bson.M{"_id": oid}, d)
// 	if err != nil {
// 		return err
// 	}
// 	return sourcedb.C(DataC).RemoveId(oid)
// }

// func (d Data) getID() (bson.ObjectId, error) {

// }

func (d Data) ToByte() []byte {
	var buffer bytes.Buffer
	err := gob.NewEncoder(&buffer).Encode(d)
	if err != nil {
		return nil
	}
	return buffer.Bytes()
}

func (mydata Data) GetRemoteMap() map[int]map[int]float64 {
	var temp []string
	var device, address, valueType, tempLen int
	var ok bool
	var err error
	var dataValue float64
	result := make(map[int]map[int]float64)
	for key, value := range mydata {
		temp = strings.Split(key, "_")
		tempLen = len(temp)
		if tempLen < 3 {
			continue
		}
		if temp[0] != "SET" {
			continue
		}
		device, err = strconv.Atoi(temp[1])
		if err != nil || device < 0 || device > 256 {
			continue
		}
		address, err = strconv.Atoi(temp[2])
		if err != nil || address < 0 || address > 256 {
			continue
		}
		if tempLen == 4 {
			valueType, err = strconv.Atoi(temp[3])
			if err != nil {
				valueType = 0
			}
		} else {
			valueType = 0
		}
		dataValue = util.GetFloat64WithDP(value, valueType)

		if _, ok = result[device]; !ok {
			result[device] = make(map[int]float64)
		}
		result[device][address] = dataValue
	}
	return result
}

// func (d Data) Save(mdb *mgo.Database, rc *redis.Client) error {
// 	id, err := d.getID()
// 	if err != nil {
// 		return err
// 	}
// 	d["_id"] = id
// 	d["_queueno"] = getQueueNo(rc)
// 	d["_processtime"] = 0
// 	d["Timestamp_Unix"] = uint32(d["Timestamp_Unix"].(float64))
// 	return mdb.C(DataC).Insert(d)
// }

// func (d Data) Upsert(mdb *mgo.Database, rc *redis.Client) error {
// 	id, err := d.getID()
// 	if err != nil {
// 		return err
// 	}
// 	d["_id"] = id
// 	d["_queueno"] = getQueueNo(rc)
// 	d["_processtime"] = 0
// 	d["Timestamp_Unix"] = uint32(d["Timestamp_Unix"].(float64))
// 	_, err = mdb.C(DataC).Upsert(bson.M{"_id": id}, d)
// 	return err
// }

// func (d Data) GetByQuery(query bson.M) []Data {
// 	return nil
// }

// func getQueueNo(rc *redis.Client) int {
// 	value, err := rc.Incr("DataQueue").Result()
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		return 0
// 	}
// 	return int(math.Mod(float64(value), float64(DataQueueNumber)))
// }

// type DataList []Data

// func (dl DataList) Paginator(mdb *mgo.Database, query bson.M, page, limit int) *util.PaginationResV2 {
// 	mgoQuery := mdb.C(DataC).Find(query)
// 	pr, err := util.MongoPaginationV2(mgoQuery, limit, page)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		return nil
// 	}
// 	err = mgoQuery.Skip(pr.Limit * (pr.Page - 1)).Limit(pr.Limit).Sort("-_id").All(&dl)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		return nil
// 	}
// 	if len(dl) == 0 {
// 		return nil
// 	}
// 	pr.Rows = dl
// 	return pr
// }

// func (dl DataList) GetTitle() []string {
// 	if len(dl) <= 0 {
// 		return nil
// 	}
// 	firstData := dl[0]
// 	var title []string
// 	for key, _ := range firstData {
// 		title = append(title, key)
// 	}
// 	return title
// }

// func (dl DataList) GetCSVTitle(mytitle []string) []string {
// 	return mytitle
// 	// var title []string
// 	// for _, key := range mytitle {
// 	// 	result := strings.Split(key, "_")
// 	// 	if len(result) < 3 {
// 	// 		title = append(title, key)
// 	// 		continue
// 	// 	}
// 	// 	d, err := strconv.Atoi(result[1])
// 	// 	if err != nil {
// 	// 		continue
// 	// 	}
// 	// 	a, err := strconv.Atoi(result[2])
// 	// 	if err != nil {
// 	// 		continue
// 	// 	}
// 	// 	key = hvac.GetFieldName(uint8(d), uint8(a))
// 	// 	title = append(title, key)
// 	// }
// 	// return title
// }

// func (dl DataList) GetData() []map[string]interface{} {
// 	dlLen := len(dl)
// 	data := make([]map[string]interface{}, dlLen)
// 	for i := 0; i < dlLen; i++ {
// 		data[i] = map[string]interface{}(dl[i])
// 	}
// 	return data
// }

// func (s *DataList) UpdateAllSync(mdb *mgo.Database, query, updateField bson.M) error {
// 	_, err := mdb.C(DataC).UpdateAll(query, bson.M{"$set": updateField})
// 	return err
// }

// func (sdl *DataList) GetSyncPaginator(mdb *mgo.Database, match bson.M, fieldList []string) *util.PaginationResV2 {
// 	limit := 200
// 	s := bson.M{}
// 	for _, field := range fieldList {
// 		s[field] = 1
// 	}
// 	mgoQuery := mdb.C(DataC).Find(match)
// 	p, err := util.MongoPaginationV2(mgoQuery, limit, 1)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		return nil
// 	}
// 	err = mgoQuery.Select(s).Skip(p.Limit * (p.Page - 1)).Limit(p.Limit).All(sdl)
// 	if err != nil {
// 		getLog().Err(err.Error())
// 		return nil
// 	}
// 	p.Rows = sdl
// 	return p

// }

// func (dl DataList) BatchSave(mdb *mgo.Database, rc *redis.Client) error {
// 	var id bson.ObjectId
// 	var err error
// 	for _, d := range dl {
// 		id, err = d.getID()
// 		if err != nil {
// 			continue
// 		}
// 		d["_id"] = id
// 		d["_queueno"] = getQueueNo(rc)
// 		d["_processtime"] = 0
// 		d["Timestamp_Unix"] = uint32(d["Timestamp_Unix"].(float64))
// 		err = mdb.C(DataC).Insert(d)
// 	}

// 	return err
// }

// func (dl DataList) BatchSaveByDate(mdb *mgo.Database) error {
// 	var id bson.ObjectId
// 	var err error
// 	var timestamp int64
// 	var t time.Time
// 	var c string
// 	for _, d := range dl {
// 		id, err = d.getID()
// 		if err != nil {
// 			continue
// 		}
// 		timestamp = int64(d["Timestamp_Unix"].(float64))
// 		t = time.Unix(timestamp, 0)
// 		c = fmt.Sprintf("%s_%s", DataC, t.Format("20060102"))
// 		d["_id"] = id
// 		d["_processtime"] = time.Now()
// 		d["Timestamp_Unix"] = timestamp
// 		_, err = mdb.C(c).Upsert(bson.M{"_id": id}, d)
// 	}
// 	getLog().Debug("batch save by date")

// 	return err
// }
