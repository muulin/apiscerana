package doc

import (
	"apiscerana/core/dao"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	sensorC     = "sensor"
	controllerC = "controller"
)

type Sensor struct {
	ID           primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	Device       int                `json:"device"`  // 載具 (0~255)
	Address      int                `json:"address"` // 位置 (0~255)
	BoxMD5       string             `json:"-"`
	SType        string             `json:"type"` // 類別 (需再拿類別清單)
	Unit         string             `json:"unit"` // 單位 (需再拿單位清單)
	DisableAlert bool               `json:"disableAlert"`
	Upper        float64            `json:"upper"`   // 警戒/限制上限
	Lower        float64            `json:"lower"`   // 警戒/限制下限
	Display      string             `json:"display"` // 自定名稱
	Frequency    struct {
		Minutes int `json:"minutes"`
		Times   int `json:"times"`
	} `json:"frequency"`
	Value        float64            `json:"value"` // 目前值/設定值
	OwnerHidden  bool               `json:"ownerHidden"`
	Stats        bool               `json:"-"` // 狀態
	EquipID      primitive.ObjectID `json:"-"`
	ProjectID    primitive.ObjectID `json:"-"`
	AlertCount   int                `json:"-"`
	UpdateTime   int                `json:"-"`
	AlertTime    int                `json:"-"`
	SetValueTime int64              `json:"-"`
	// EquipmentObj []Equipment        `json:"-" bson:"lookupEquipment,omitempty"`
	// ProjectObj   []Project          `json:"-" bson:"lookupProject,omitempty"`
	HasAlert  bool `json:"-"`
	Sort      int  `json:"-"`
	ValueType int  `json:"-"` // 數值的處理0: 原數值; 1: 數值 乘或除10; 2: 數值 乘或除100

	dao.CommonDoc `bson:"meta"`
}

func (s *Sensor) GatewayID() primitive.ObjectID {
	b := [12]byte(s.ID)
	b[9] = b[5]
	b[8] = b[4]
	b[7] = b[3]
	b[6] = b[2]
	b[5] = b[1]
	b[4] = b[0]
	b[0] = 0
	b[1] = 0
	b[2] = 0
	b[3] = 0
	b[10] = 0
	b[11] = 0
	return b
}

func (s *Sensor) newID() primitive.ObjectID {
	var b [12]byte
	if s.BoxMD5 == "" {
		return b
	}

	boxID := []byte(s.BoxMD5)
	for i := 0; i < 10; i++ {
		b[i] = boxID[i]
	}

	// device & address for 2 bytes
	b[10] = byte(uint8(s.Device))
	b[11] = byte(uint8(s.Address))
	return b
}

func (s *Sensor) GetC() string {
	return sensorC
}

func (s *Sensor) GetDoc() interface{} {
	if s.ID.IsZero() {
		s.ID = s.newID()
	}
	return s
}

func (s *Sensor) GetID() primitive.ObjectID {
	return s.ID
}

func (s *Sensor) GetIndexes() []mongo.IndexModel {
	return nil
}

type Controller struct {
	Sensor `bson:",inline"`
}

func (c *Controller) GetC() string {
	return controllerC
}

type SensorAggrEquipProject struct {
	Sensor
	EquipmentObj Equipment `json:"-" bson:"lookupEquipment,omitempty"`
	ProjectObj   Project   `json:"-" bson:"lookupProject,omitempty"`
}

func (aggr SensorAggrEquipProject) GetPipeline(q bson.M) mongo.Pipeline {
	return mongo.Pipeline{
		bson.D{{Key: "$match", Value: q}},
		bson.D{{Key: "$lookup", Value: bson.D{
			{Key: "from", Value: equipmentC},
			{Key: "localField", Value: "equipid"},
			{Key: "foreignField", Value: "_id"},
			{Key: "as", Value: "lookupEquipment"}},
		}},
		bson.D{{Key: "$lookup", Value: bson.D{
			{Key: "from", Value: projectC},
			{Key: "localField", Value: "projectid"},
			{Key: "foreignField", Value: "_id"},
			{Key: "as", Value: "lookupProject"}},
		}},
		bson.D{{
			Key: "$unwind", Value: bson.D{
				{Key: "path", Value: "$lookupEquipment"},
				{Key: "preserveNullAndEmptyArrays", Value: false},
			},
		}},
		bson.D{{
			Key: "$unwind", Value: bson.D{
				{Key: "path", Value: "$lookupProject"},
				{Key: "preserveNullAndEmptyArrays", Value: false},
			},
		}},
	}
}
func (aggr SensorAggrEquipProject) GetC() string {
	return sensorC
}
