package doc

import (
	"apiscerana/core/dao"

	"github.com/globalsign/mgo/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	equipLogC          = "logEquipment"
	LogEquipTagJob     = "job"
	LogEquipTagSetting = "setting"
	LogEquipTagAlarm   = "alarm"
	LogEquipTagAlert   = "alert"
	LogEquipTagRemote  = "remote"

	addSensorTpl   = "{{new}}: %s"
	delSensorTpl   = "{{del}}: %s"
	upperSensorTpl = "{{upper}}: %.1f"
	lowerSensorTpl = "{{lower}}: %.1f"
)

type LogEquipment struct {
	ID          primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	Tag         string
	Timestamp   int64
	EquipName   string
	ProjectName string
	Content     string
	FormatType  string // 格式化類型供前端
	Value       string
	Detail      []string
	EquipID     primitive.ObjectID
	ProjectID   primitive.ObjectID

	dao.CommonDoc `bson:"meta"`
}

func (u *LogEquipment) GetDoc() interface{} {
	if u.ID.IsZero() {
		u.ID = primitive.NewObjectID()
	}
	return u
}

func (u *LogEquipment) GetC() string {
	return equipLogC
}

func (u *LogEquipment) GetID() primitive.ObjectID {
	return u.ID
}

func (u *LogEquipment) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{
		{
			Keys: bson.D{
				{Name: "Tag", Value: 1},
				{Name: "Timestamp", Value: -1},
				{Name: "EquipID", Value: 1},
			},
		},
	}
}
