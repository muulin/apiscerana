package doc

import (
	"apiscerana/core/dao"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	equipmentC = "equipment"
)

type Equipment struct {
	ID      primitive.ObjectID `bson:"_id"`
	Name    string
	Model   string
	EquipID string
	EMSID   uint8
	Box     struct {
		MACaddress string
		BoxID      string
	}
	ProjectID      primitive.ObjectID
	Sensors        []Sensor               `json:"sensors" bson:"lookupSensors,omitempty"`
	Controllers    []Sensor               `json:"controllers" bson:"lookupControllers,omitempty"`
	DelSensors     []Sensor               `json:"delSensors,omitempty" bson:"-"`
	DelControllers []Sensor               `json:"delControllers,omitempty" bson:"-"`
	AdditionInfo   map[string]interface{} `json:"-"`
	Sort           int                    `json:"-"`

	NecSync struct {
		EquipCode string
	} `bson:"nec-sync"`

	SensorFolder     []folder `json:"sensorFolder"`
	ControllerFolder []folder `json:"controlFolder"`

	ProjectObj []Project `json:"-" bson:"lookupProoject,omitempty"`

	dao.CommonDoc `bson:"meta"`
}

func (e *Equipment) GetC() string {
	return gatewayC
}

func (e *Equipment) GetDoc() interface{} {
	if e.ID.IsZero() {
		e.ID = primitive.NewObjectID()
	}
	return e
}

func (e *Equipment) GetID() primitive.ObjectID {
	return e.ID
}

func (e *Equipment) GetIndexes() []mongo.IndexModel {
	return nil
}

type folder struct {
	Name    string   `json:"name"`
	Sensors []string `json:"sensors"`
}
