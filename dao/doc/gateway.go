package doc

import (
	"apiscerana/core/dao"
	"apiscerana/util"
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	gatewayC = "gateway"
)

type Gateway struct {
	ID                primitive.ObjectID `json:"id,omitempty" bson:"_id"`
	Name              string
	MACaddress        string
	GwID              string
	Conf              map[string]interface{}
	State             string
	StartTimestamp    int64
	EndTimestamp      int64
	TestTimes         uint8
	ModifyTimestamp   int64
	UpdateTimestamp   int64
	ProjectID         primitive.ObjectID  `json:"-"`
	Permission        []gatewayPermission `json:"-"`
	ConfirmPermission []confirmPermission `json:"-"`
	FirmwareVersion   string              `json:"-"`
	IsReplaced        bool                `json:"-"`
	ReplaceID         *primitive.ObjectID `json:"-"`
	ReplaceMAC        string              `json:"-"`
	ReplaceTime       int64               `json:"-"`
	IsAutoUpdate      bool
	Timezone          string

	IsSync        bool
	SyncFieldList []string

	dao.CommonDoc `bson:"meta"`
}

func (gw *Gateway) NewID() primitive.ObjectID {
	md5 := GetBoxMD5(gw.MACaddress, gw.GwID)
	var b [12]byte
	// boxmd5, first 6 bytes of md5(hostname)
	boxId := []byte(md5)
	b[0] = 0
	b[1] = 0
	b[2] = 0
	b[3] = 0
	b[4] = boxId[0]
	b[5] = boxId[1]
	b[6] = boxId[2]
	b[7] = boxId[3]
	b[8] = boxId[4]
	b[9] = boxId[5]
	b[10] = 0
	b[11] = 0
	return b
}

func (g *Gateway) GetC() string {
	return gatewayC
}

func (g *Gateway) GetDoc() interface{} {
	if g.ID.IsZero() {
		g.ID = g.NewID()
	}
	return g
}

func (g *Gateway) GetID() primitive.ObjectID {
	return g.ID
}

func (s *Gateway) GetIndexes() []mongo.IndexModel {
	return nil
}

type confirmPermission struct {
	Email      string `json:"email"`
	Permission string `json:"permission"`
}

type gatewayPermission struct {
	Account    string `json:"account"`
	Email      string `json:"email"`
	Name       string `json:"name"`
	Permission string `json:"permission"`
}

func GetBoxMD5(mac, gwid string) string {
	return util.MD5(fmt.Sprintf("%s%s", mac, gwid))
}
