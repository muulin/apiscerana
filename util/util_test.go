package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_StrAppend(t *testing.T) {
	r := StrAppend("aa", "bbb")
	assert.Equal(t, "aabbb", r)
}

func Test_IntToFixStrLen(t *testing.T) {

	r, err := IntToFixStrLen(1, 5)
	assert.Equal(t, "00001", r)
	assert.Nil(t, err)

	r, err = IntToFixStrLen(12345, 5)
	assert.Equal(t, "12345", r)
	assert.Nil(t, err)

	r, err = IntToFixStrLen(12345, 3)
	assert.Equal(t, "", r)
	assert.NotNil(t, err)
}
