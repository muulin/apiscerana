package mock

type BasicMock map[string]interface{}

func (mm BasicMock) GetMockFunc(name string) interface{} {
	f, ok := mm[name]
	if !ok {
		panic("not set " + name + " func")
	}
	return f
}

func (mm *BasicMock) SetMockFunc(name string, f interface{}) {
	(*mm)[name] = f
}
