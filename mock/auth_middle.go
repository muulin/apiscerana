package mock

import (
	"apiscerana/api/input"
	"apiscerana/core/api"
	"apiscerana/util"
	"net/http"
)

func NewMockAuthMiddle(name string) api.AuthMidInter {
	return &mockAuthMiddle{
		Name: name,
	}
}

type mockAuthMiddle struct {
	Name string
}

func (mam *mockAuthMiddle) SetAllowSys(sys string) {

}

func (mam mockAuthMiddle) GetName() string {
	return mam.Name
}

func (mam *mockAuthMiddle) AddAuthPath(path string, method string, auth bool, group []string) {

}

func (mam *mockAuthMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			r.Header.Add(api.HeaderAuthSys, "mock-auth")
			r.Header.Set("isLogin", "true")
			ru := input.NewReqUser(
				"muulin",
				"peter",
				"pid",
				"admin",
			)
			r = util.SetCtxKeyVal(r, input.CtxUserInfoKey, ru)
			r.Header.Add(api.HeaderAuthSys, "mock-auth")
			f(w, r)
		}
	}
}
