package mock

import "time"

type MockRedis struct {
	BasicMock
}

func (mr *MockRedis) Close() error {
	f := mr.GetMockFunc("Close")
	execFunc := f.(func() error)
	return execFunc()
}

func (mr *MockRedis) Ping() string {
	f := mr.GetMockFunc("Ping")
	execFunc := f.(func() string)
	return execFunc()
}

func (mr *MockRedis) Set(k string, v interface{}, exp time.Duration) (string, error) {
	f := mr.GetMockFunc("Set")
	execFunc := f.(func(k string, v interface{}, exp time.Duration) (string, error))
	return execFunc(k, v, exp)
}

func (mr *MockRedis) Del(k string) (int64, error) {
	f := mr.GetMockFunc("Del")
	execFunc := f.(func(k string) (int64, error))
	return execFunc(k)
}

func (mr *MockRedis) LPush(k string, v interface{}) (int64, error) {
	f := mr.GetMockFunc("LPush")
	execFunc := f.(func(k string, v interface{}) (int64, error))
	return execFunc(k, v)
}

func (mr *MockRedis) RPop(k string) ([]byte, error) {
	f := mr.GetMockFunc("RPop")
	execFunc := f.(func(k string) ([]byte, error))
	return execFunc(k)
}
