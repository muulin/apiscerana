package mock

import (
	"apiscerana/core/dao"
	"apiscerana/util"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mockMongo struct {
	funcMap map[string]interface{}
}

func NewMockMongo(funcMap map[string]interface{}) mockMongo {
	return mockMongo{
		funcMap: funcMap,
	}
}

func (mm mockMongo) getMockFunc(name string) interface{} {
	f, ok := mm.funcMap[name]
	if !ok {
		panic("not set FindOneFunc")
	}
	return f
}

func (mm *mockMongo) SetMockFunc(name string, f interface{}) {
	mm.funcMap[name] = f
}

func (mm mockMongo) Save(d dao.DocInter, u dao.LogUser) (primitive.ObjectID, error) {
	return primitive.NewObjectID(), nil
}
func (mm mockMongo) RemoveByID(d dao.DocInter, u dao.LogUser) (int64, error) {
	return 0, nil
}
func (mm mockMongo) UpdateOne(d dao.DocInter, fields bson.D, u dao.LogUser) (int64, error) {
	return 0, nil
}
func (mm mockMongo) FindByID(d dao.DocInter) error {
	return nil
}
func (mm mockMongo) FindOne(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
	f := mm.getMockFunc("FindOne")
	execFunc := f.(func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error)
	return execFunc(d, q, opts...)
}

func (mm mockMongo) Find(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
	f := mm.getMockFunc("Find")
	execFunc := f.(func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error))
	return execFunc(d, q, opts...)
}
func (mm mockMongo) PipeFindOne(aggr dao.MgoAggregate, filter bson.M) error {
	return nil
}
func (mm mockMongo) PipeFind(aggr dao.MgoAggregate, filter bson.M) (interface{}, error) {
	return nil, nil
}
func (mm mockMongo) PagePipeFind(aggr dao.MgoAggregate, filter bson.M, limit, page int64) (interface{}, error) {
	return nil, nil
}
func (mm mockMongo) PageFind(d dao.DocInter, q bson.M, limit, page int64) (interface{}, error) {
	return nil, nil
}
func (mm mockMongo) CountDocuments(d dao.DocInter, q bson.M) (int64, error) {
	return 0, nil
}
func (mm mockMongo) GetPaginationSource(d dao.DocInter, q bson.M) util.PaginationSource {
	return nil
}

func (mm mockMongo) CountAggrDocuments(d dao.MgoAggregate, q bson.M) (int64, error) {
	return 0, nil
}
func (mm mockMongo) GetAggrPaginationSource(aggr dao.MgoAggregate, q bson.M) util.PaginationSource {
	return nil
}
