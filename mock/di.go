package mock

import (
	"apiscerana/core"
	"apiscerana/core/api"
	"apiscerana/core/db"
	"apiscerana/core/log"
	"context"

	"github.com/gorilla/mux"
)

func NewMockDI(funcMap map[string]interface{}) core.DI {
	return &mockDI{
		BasicMock: funcMap,
	}
}

type mockDI struct {
	BasicMock
}

func (m *mockDI) GetConfigFile(fn string) (string, error) {
	f := m.GetMockFunc("GetConfigFile")
	execFunc := f.(func(fn string) (string, error))
	return execFunc(fn)
}

func (m *mockDI) GetJwt() api.Jwt {
	return nil
}

func (m *mockDI) NewLogger(key string) log.Logger {
	return nil
}

func (d *mockDI) HasMongoDbConf() bool {
	return false
}

func (m *mockDI) NewMongoDBClient(ctx context.Context) (db.MongoDBClient, error) {
	return nil, nil
}

func (m *mockDI) NewRedisClient(ctx context.Context) (db.RedisClient, error) {
	return nil, nil
}

func (m *mockDI) RunAPI(authMiddle api.AuthMidInter, middles []api.Middle, apis ...api.API) {
	panic("not support")
}

func (m *mockDI) GetRouter(authMiddle api.AuthMidInter, middles []api.Middle, apis ...api.API) *mux.Router {
	r := mux.NewRouter()
	var mws []api.Middleware
	for _, m := range middles {
		mws = append(mws, m.GetMiddleWare())
	}
	for _, myapi := range apis {
		myapi.Init()
		for _, handler := range myapi.GetAPIs() {
			if authMiddle != nil {
				authMiddle.AddAuthPath(handler.Path, handler.Method, handler.Auth, handler.Group)
			}
			r.HandleFunc(handler.Path, api.BuildChain(handler.Next, mws...)).Methods(handler.Method)
		}
	}
	return r
}
