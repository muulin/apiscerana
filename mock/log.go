package mock

import (
	"apiscerana/core/log"
	"fmt"
)

type mockLogger string

func NewLogger() log.Logger {
	return mockLogger("mock")
}

func (ml mockLogger) Info(msg string) {
	fmt.Sprintln(msg)
}
func (ml mockLogger) Debug(msg string) {
	fmt.Sprintln(msg)
}
func (ml mockLogger) Warn(msg string) {
	fmt.Sprintln(msg)
}
func (ml mockLogger) Err(msg string) {
	fmt.Sprintln(msg)
}

func (ml mockLogger) Fatal(msg string) {
	fmt.Sprintln(msg)
}
