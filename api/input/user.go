package input

import (
	"apiscerana/core/dao"
	"apiscerana/dao/doc"
	"apiscerana/util"

	"errors"
	"net/http"
)

const (
	CtxUserInfoKey = util.CtxKey("userInfo")
)

type ReqUser interface {
	dao.LogUser
	GetPerm() string
	GetPhoneID() string
}

type reqUserImpl struct {
	Account string
	Name    string
	Perm    string
	PID     string
}

func (ru reqUserImpl) GetName() string {
	return ru.Name
}

func (ru reqUserImpl) GetAccount() string {
	return ru.Account
}

func (ru reqUserImpl) GetPerm() string {
	return ru.Perm
}

func (ru reqUserImpl) GetPhoneID() string {
	return ru.PID
}

func NewReqUser(acc, name, pid, perm string) ReqUser {
	return reqUserImpl{
		Account: acc,
		Name:    name,
		PID:     pid,
		Perm:    perm,
	}
}

func GetUserInfo(req *http.Request) ReqUser {
	ctx := req.Context()
	reqID := ctx.Value(CtxUserInfoKey)
	if ret, ok := reqID.(ReqUser); ok {
		return ret
	}
	return nil
}

type LoginV2 struct {
	Account  string
	Pwd      string
	PhoneID  string
	Platform string
	Push     string
	SysCode  string
}

func (qb *LoginV2) Validate() error {
	if qb.SysCode == "" {
		return errors.New("invalid System")
	}
	if qb.Account == "" {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}
	if qb.PhoneID == "" {
		return errors.New("miss phoneID")
	}
	if qb.Push != "" {
		if !util.IsStrInList(qb.Platform,
			doc.PlatformAndroid,
			doc.PlatformIOS,
			doc.PlatformWEB,
			doc.PlatformDesk) {
			return errors.New("paltform error")
		}
	}
	return nil
}

type Register struct {
	Account string
	Pwd     string
	Perm    string
	Key     string
}

func (qb *Register) Validate() error {
	if qb.Account == "" {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss pwd")
	}
	if qb.Perm == "" {
		return errors.New("miss perm")
	}
	if qb.Key == "" {
		return errors.New("miss key")
	}
	return nil
}

type ChangePwd struct {
	New, Old string
}

func (qb *ChangePwd) Validate() error {
	if qb.New == "" {
		return errors.New("missing new pwd")
	}
	if qb.Old == "" {
		return errors.New("missing old pwd")
	}
	return nil
}
