package middle

import (
	"net/http"
	"runtime"

	"apiscerana/core"
	"apiscerana/core/db"
	"apiscerana/core/log"
	"apiscerana/util"

	"github.com/google/uuid"
)

type DBMiddle string

func (am DBMiddle) GetName() string {
	return string(am)
}

func (am DBMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			uuid := uuid.New().String()
			di := core.GetDI()
			l := di.NewLogger(uuid)
			r = util.SetCtxKeyVal(r, log.CtxLogKey, l)
			if !di.HasMongoDbConf() {
				panic("mongo not set")
			}
			ctx := r.Context()
			dbclt, err := di.NewMongoDBClient(ctx)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}
			r = util.SetCtxKeyVal(r, db.CtxMongoKey, dbclt)

			redisclt, err := di.NewRedisClient(ctx)
			if err != nil {
				l.Warn(err.Error())
			} else {
				r = util.SetCtxKeyVal(r, db.CtxRedisKey, redisclt)
			}

			// elsclt := di.GetSearch()
			// r = util.SetCtxKeyVal(r, db.CtxElasticKey, elsclt)

			f(w, r)
			dbclt.Close()
			runtime.GC()
		}
	}
}

type mockDBMiddle struct {
	log          log.Logger
	mockModelMap map[util.CtxKey]interface{}
}

func NewMockDBMiddle(l log.Logger, mockModelMap map[util.CtxKey]interface{}) *mockDBMiddle {
	return &mockDBMiddle{
		log:          l,
		mockModelMap: mockModelMap,
	}
}

func (am *mockDBMiddle) GetName() string {
	return string("mockDB")
}

func (am *mockDBMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			r = util.SetCtxKeyVal(r, log.CtxLogKey, am.log)
			for k, v := range am.mockModelMap {
				r = util.SetCtxKeyVal(r, k, v)
			}
			f(w, r)
		}
	}
}
