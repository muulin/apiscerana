package v2

import (
	"apiscerana/api/input"
	"apiscerana/api/middle"
	"apiscerana/core"
	"apiscerana/core/api"
	"apiscerana/dao/doc"
	"apiscerana/mock"
	"apiscerana/util"
	"net/http"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/steinfletcher/apitest"
)

func Test_LogoutEndpoint(t *testing.T) {
	ac := api.NewTestApiConf()
	l := mock.NewLogger()
	authMiddle := mock.NewMockAuthMiddle("auth")
	authModel := mock.MockAuthModel{
		BasicMock: map[string]interface{}{
			"Logout": func(u input.ReqUser, token string) error {
				return nil
			},
		},
	}
	mockdb := middle.NewMockDBMiddle(l, map[util.CtxKey]interface{}{
		mock.CtxMockAuthModel: authModel,
	})
	r := ac.GetRouter(
		authMiddle,
		[]api.Middle{
			authMiddle,
			mockdb,
		},
		AuthAPI("authV2"),
	)
	apitest.New().Handler(r).Post("/v2/logout").
		Expect(t).Status(http.StatusOK).End()
}

func Test_LoginEndpoint(t *testing.T) {
	mj := mock.MockJWT{
		BasicMock: map[string]interface{}{
			"Parse": func(tokenStr string) (*jwt.Token, error) {
				return nil, nil
			},
		}}
	core.SetDI(mock.NewMockDI(
		map[string]interface{}{
			"GetJwt": func() api.Jwt {
				return mj
			},
		},
	))

	ac := api.NewTestApiConf()
	l := mock.NewLogger()
	authMiddle := mock.NewMockAuthMiddle("auth")
	authModel := mock.MockAuthModel{
		BasicMock: map[string]interface{}{
			"Login": func(i input.LoginV2) (*doc.User, error) {
				return nil, nil
			},
		},
	}
	mockdb := middle.NewMockDBMiddle(l, map[util.CtxKey]interface{}{
		mock.CtxMockAuthModel: authModel,
	})

	// 測試已登入
	r := ac.GetRouter(
		authMiddle,
		[]api.Middle{
			authMiddle,
			mockdb,
		},
		AuthAPI("authV2"),
	)
	apitest.New().Handler(r).Post("/v2/login").
		Expect(t).Status(http.StatusAccepted).End()

	// 測試找不到使用者，拿到403
	r = ac.GetRouter(
		nil,
		[]api.Middle{
			mockdb,
		},
		AuthAPI("authV2"),
	)
	apitest.New().Handler(r).Post("/v2/login").
		Expect(t).Status(http.StatusBadRequest).End()

	apitest.New().Handler(r).Post("/v2/login").Header("Auth-Sys", "testauthsys").
		FormData("account", "peter").FormData("pwd", "peter").
		FormData("phoneID", "peter").FormData("platform", "peter").
		FormData("token", "peter").
		Expect(t).Status(http.StatusForbidden).End()
}
