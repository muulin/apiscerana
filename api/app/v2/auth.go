package v2

import (
	"encoding/json"
	"net/http"

	"apiscerana/api/input"
	"apiscerana/api/middle"
	"apiscerana/core"
	"apiscerana/core/api"
	"apiscerana/model"
	"apiscerana/util"
)

type AuthAPI string

func (a AuthAPI) GetName() string {
	return string(a)
}

func (a AuthAPI) Init() {

}

func (a AuthAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		{Path: "/v2/login", Next: a.LoginEndpoint, Method: "POST", Auth: false},
		{Path: "/v2/logout", Next: a.LogoutEndpoint, Method: "POST", Auth: true},
	}
}

func (a AuthAPI) LoginEndpoint(w http.ResponseWriter, req *http.Request) {
	// 判斷使用者是否有token (末實作)
	// 從資料庫取出使用者帳號資料
	// 從Redis取出private key
	// 解密碼出來做MD5比對使用者密碼
	// 成功 - 產生token 將使用者資訊加密存入redis方便取用
	// 寫入redis
	isLogin := util.IsLogin(req)
	if isLogin {
		w.WriteHeader(http.StatusAccepted)
		return
	}

	postValue, err := util.GetPostValue(req, true, []string{"account", "pwd", "push", "platform", "phoneID"})
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	i := input.LoginV2{
		Account:  postValue["account"].(string),
		Pwd:      postValue["pwd"].(string),
		PhoneID:  postValue["phoneID"].(string),
		Platform: postValue["platform"].(string),
		Push:     postValue["push"].(string),
		SysCode:  req.Header.Get(api.HeaderAuthSys),
	}

	if err = i.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	showInfo := false
	queryValues := util.GetQueryValue(req, []string{"info"}, false)
	if info, ok := queryValues["info"]; ok && info == "true" {
		showInfo = true
	}

	am := model.NewAuthModelByReq(req)
	u, err := am.Login(i)
	if err != nil {
		if apierr, ok := err.(api.ApiError); ok {
			w.WriteHeader(apierr.GetStatus())
			w.Write([]byte(err.Error()))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	if u == nil {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	jwt := core.GetDI().GetJwt()
	t, err := jwt.GetToken(map[string]interface{}{
		"sub": u.Account,
		"nam": u.Name,
		"per": u.Group,
		"pid": i.PhoneID,
	})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("get token error: " + err.Error()))
		return
	}

	req.Header.Add("SET_TOKEN", *t)

	w.Header().Set("Content-Type", "application/json")
	if showInfo {
		json.NewEncoder(w).Encode(
			map[string]string{
				"token":   *t,
				"group":   u.Group,
				"company": u.Company,
				"name":    u.Name,
			})
	} else {
		json.NewEncoder(w).Encode(
			map[string]string{
				"token": *t,
				"name":  u.Name,
			})
	}
}

func (api AuthAPI) LogoutEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)

	am := model.NewAuthModelByReq(req)

	token := req.Header.Get(middle.AuthTokenKey)

	am.Logout(ui, token)

	w.WriteHeader(http.StatusOK)
}
