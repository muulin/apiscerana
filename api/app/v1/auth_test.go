package v1

import (
	"apiscerana/api/middle"
	"apiscerana/core/api"
	"apiscerana/dao/doc"
	"apiscerana/mock"
	"apiscerana/util"

	"net/http"
	"testing"

	"github.com/steinfletcher/apitest"
	jsonpath "github.com/steinfletcher/apitest-jsonpath"
)

func Test_AppEndpoint(t *testing.T) {
	ac := api.NewTestApiConf()
	l := mock.NewLogger()
	authMiddle := mock.NewMockAuthMiddle("auth")
	appModel := mock.MockAppModel{
		BasicMock: map[string]interface{}{
			"CheckAppVers": func(app, version string) *doc.Version {
				return nil
			},
			"CheckAppLangVers": func(lang, version string) *doc.Version {
				return nil
			},
			"CheckDeskLangVers": func(lang, version string) *doc.Version {
				return nil
			},
			"IsAppReview": func(platform, version string) bool {
				return false
			},
		},
	}
	mockdb := middle.NewMockDBMiddle(l, map[util.CtxKey]interface{}{
		mock.CtxMockAppModel: appModel,
	})
	r := ac.GetRouter(
		authMiddle,
		[]api.Middle{
			authMiddle,
			mockdb,
		},
		AuthAPI("authV1"),
	)
	apitest.New().Handler(r).Get("/v1/app").QueryParams(map[string]string{
		"app":    "ios",
		"app-v":  "1.0.0",
		"lang":   "en",
		"lang-v": "16.0",
	}).Expect(t).Status(http.StatusOK).End()

	// testing missing params app
	apitest.New().Handler(r).Get("/v1/app").QueryParams(map[string]string{
		"app-v":  "1.0.0",
		"lang":   "en",
		"lang-v": "16.0",
	}).Expect(t).
		Status(http.StatusBadRequest).
		End()

	// test return app version
	appModel.SetMockFunc("CheckAppVers", func(app, version string) *doc.Version {
		return &doc.Version{
			Version: "1.1.0",
			Url:     "checkAppVersUrl",
		}
	})
	appModel.SetMockFunc("CheckAppLangVers", func(app, version string) *doc.Version {
		return &doc.Version{
			Version: "12.0",
			Url:     "checkAppLangUrl",
		}
	})

	apitest.New().Handler(r).Get("/v1/app").QueryParams(map[string]string{
		"app":    "ios",
		"app-v":  "1.0.0",
		"lang":   "en",
		"lang-v": "16.0",
	}).Expect(t).Status(http.StatusOK).
		Assert(jsonpath.Equal(`$.appURL`, "checkAppVersUrl")).
		Assert(jsonpath.Equal(`$.appV`, "1.1.0")).
		Assert(jsonpath.Equal(`$.langURL`, "checkAppLangUrl")).
		Assert(jsonpath.Equal(`$.langV`, "12.0")).
		End()

}
