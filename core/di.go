package core

import (
	"apiscerana/core/api"
	"apiscerana/core/db"
	"apiscerana/core/log"
	"apiscerana/core/que"
	"apiscerana/util"
	"context"
	"errors"
	"path/filepath"

	"io/ioutil"

	"github.com/gorilla/mux"
	yaml "gopkg.in/yaml.v2"
)

type DI interface {
	RunAPI(authMiddle api.AuthMidInter, middles []api.Middle, apis ...api.API)
	GetRouter(authMiddle api.AuthMidInter, middles []api.Middle, apis ...api.API) *mux.Router
	HasMongoDbConf() bool
	NewMongoDBClient(ctx context.Context) (db.MongoDBClient, error)
	NewRedisClient(ctx context.Context) (db.RedisClient, error)
	GetJwt() api.Jwt
	NewLogger(key string) log.Logger
	GetConfigFile(fn string) (string, error)
}

type diImpl struct {
	//Redis *db.Redis  `yaml:"redis,omitempty"`
	*log.LogConf `yaml:"log,omitempty"`
	*api.APIConf `yaml:"api,omitempty"`
	*api.JwtConf `yaml:"jwtConf"`

	*db.RedisConf `yaml:"redis"`
	*db.MongoConf `yaml:"mongodb"`

	*que.QueConf `yaml:"rabbitmq,omitempty"`

	confPath string
}

var mydi DI

func GetDI() DI {
	if mydi == nil {
		panic("not init di")
	}
	return mydi
}

func InitConfByPath(p string) {
	yamlFile, err := ioutil.ReadFile(p + "/config.yml")
	if err != nil {
		panic(err)
	}
	d := &diImpl{
		confPath: p,
	}
	err = yaml.Unmarshal(yamlFile, d)
	if err != nil {
		panic(err)
	}
	if d.MongoConf == nil {
		panic("missing mongo config")
	}
	if d.JwtConf == nil {
		panic("missing jwt config")
	}
	d.JwtConf.SetConfPath(p)
	if d.APIConf == nil {
		panic("missing api config")
	}
	mydi = d
	util.InitValidator()
}

func (d *diImpl) GetConfigFile(fn string) (string, error) {
	filename, err := filepath.Abs(util.StrAppend(d.confPath, "/", fn))
	if err != nil {
		return "", err
	}

	exist, _ := util.FileExists(filename)
	if !exist {
		return "", errors.New("file not exists: " + filename)
	}
	return filename, nil
}

func (d *diImpl) HasMongoDbConf() bool {
	return d.MongoConf != nil
}

func SetDI(d DI) {
	mydi = d
}
