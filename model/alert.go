package model

import (
	"apiscerana/core/db"
	"apiscerana/core/log"
	"apiscerana/util"
	"net/http"
)

func NewAlertModelByReq(req *http.Request) *appModel {
	l := util.GetCtxVal(req, log.CtxLogKey)

	m := &appModel{
		mdbm: NewMgoModelByReq(req, db.CoreDB),
		log:  l.(log.Logger),
	}
	m.load()
	return m
}

type alertModel struct {
	mdbm MgoDBModel
	log  log.Logger
}
