package model

import (
	"apiscerana/api/input"
	"apiscerana/core"
	"apiscerana/core/api"
	"apiscerana/core/db"
	"apiscerana/core/log"
	"apiscerana/dao/doc"
	"apiscerana/mock"
	"apiscerana/util"
	"io/ioutil"

	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/yaml.v2"
)

type AppModel interface {
	IsAppReview(platform, version string) bool
	CheckAppVers(app, version string) *doc.Version
	CheckAppLangVers(lang, version string) *doc.Version
	CheckDeskLangVers(lang, version string) *doc.Version
}

func NewAppModel(mdbm MgoDBModel, l log.Logger) AppModel {
	m := &appModel{
		mdbm: mdbm,
		log:  l,
	}
	m.load()
	return m
}

func NewAppModelByReq(req *http.Request) AppModel {
	mapp := util.GetCtxVal(req, mock.CtxMockAppModel)
	if m, ok := mapp.(mock.MockAppModel); ok {
		return &m
	}

	l := util.GetCtxVal(req, log.CtxLogKey)
	m := &appModel{
		mdbm: NewMgoModelByReq(req, db.CoreDB),
		log:  l.(log.Logger),
	}
	m.load()
	return m
}

type appModel struct {
	mdbm    MgoDBModel
	log     log.Logger
	appInfo *doc.App
}

func (am *appModel) IsAppReview(platform, version string) bool {
	if am.appInfo == nil {
		return false
	}
	if platform != doc.PlatformIOS {
		return false
	}
	return version == am.appInfo.IOSReviw
}

func (am *appModel) CheckAppVers(app, version string) *doc.Version {
	if am.appInfo == nil {
		return nil
	}
	var v doc.Version
	switch app {
	case doc.PlatformIOS:
		v = am.appInfo.IOS
	case doc.PlatformAndroid:
		v = am.appInfo.Android
	case doc.PlatformDesk:
		v = am.appInfo.Desk
	}
	if v.IsValid(version) {
		return nil
	}
	return &v
}

func (am *appModel) CheckDeskLangVers(lang, version string) *doc.Version {
	if am.appInfo == nil {
		return nil
	}
	v, ok := am.appInfo.DeskLang[lang]
	if !ok {
		return nil
	}
	if v.Version == version {
		return nil
	}
	return &v
}

func (am *appModel) CheckAppLangVers(lang, version string) *doc.Version {
	if am.appInfo == nil {
		return nil
	}
	v, ok := am.appInfo.Lang[lang]
	if !ok {
		return nil
	}
	if v.Version == version {
		return nil
	}

	return &v
}

func (am *appModel) load() {
	sort := options.FindOne().SetSort(bson.M{"_id": -1})
	app := &doc.App{}
	err := am.mdbm.FindOne(app, bson.M{}, sort)
	if !app.ID.IsZero() {
		am.appInfo = app
		return
	}
	// load from file
	filename, err := core.GetDI().GetConfigFile("app.yml")
	if err != nil {
		am.log.Err("file not exist: " + filename)
		return
	}
	yamlFile, err := ioutil.ReadFile(filename)

	if err != nil {
		am.log.Err(err.Error())
		return
	}
	err = yaml.Unmarshal(yamlFile, &app)
	if err != nil {
		am.log.Err(err.Error())
		return
	}
	am.appInfo = app
	am.mdbm.Save(app, nil)
}

type AuthModel interface {
	Login(i input.LoginV2) (*doc.User, error)
	Logout(u input.ReqUser, token string) error
}

func NewAuthModel(mdbm MgoDBModel, redis db.RedisClient, l log.Logger) AuthModel {
	m := &authModel{
		mdbm:  mdbm,
		redis: redis,
		log:   l,
	}
	return m
}

func NewAuthModelByReq(req *http.Request) AuthModel {
	mapp := util.GetCtxVal(req, mock.CtxMockAuthModel)
	if m, ok := mapp.(mock.MockAuthModel); ok {
		return &m
	}

	l := util.GetCtxVal(req, log.CtxLogKey)

	m := &authModel{
		mdbm:  NewMgoModelByReq(req, db.CoreDB),
		redis: db.GetCtxRedis(req),
		log:   l.(log.Logger),
	}
	return m
}

type authModel struct {
	mdbm    MgoDBModel
	redis   db.RedisClient
	log     log.Logger
	appInfo *doc.App
}

func (am *authModel) Login(i input.LoginV2) (*doc.User, error) {
	u := &doc.User{}
	err := am.mdbm.FindOne(u, bson.M{
		"account": i.Account,
		"pwd":     util.MD5(i.Pwd),
		"sys":     bson.M{"$elemMatch": bson.M{"syscode": i.SysCode}},
	})
	if err != nil {
		return nil, api.NewApiError(http.StatusNotFound, "account or password error")
	}
	group := u.GetGroup(i.SysCode)
	if group == "" || u.Enable == false {
		return nil, api.NewApiError(http.StatusForbidden, "Permission denied")
	}
	u.Group = group
	// Save User Push Token
	upt := &doc.UserPushToken{
		PushToken: i.Push,
		Account:   u.Account,
		Sys:       i.SysCode,
		Platform:  i.Platform,
		PhoneID:   i.PhoneID,
	}
	am.mdbm.Save(upt, u)

	return u, nil
}

func (am *authModel) Logout(u input.ReqUser, token string) error {
	upt := &doc.UserPushToken{}
	err := am.mdbm.FindOne(upt, bson.M{"phoneid": u.GetPhoneID})
	if !upt.ID.IsZero() {
		am.mdbm.RemoveByID(upt, u)
	}

	if am.redis == nil {
		return nil
	}
	_, err = am.redis.Del(util.MD5(token))
	if err != nil {
		am.log.Warn(err.Error())
		return err
	}
	return nil
}
