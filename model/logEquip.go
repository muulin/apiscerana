package model

import (
	"apiscerana/api/input"
	"apiscerana/core/log"
	"apiscerana/dao/doc"
)

type LogEquipDoc interface {
	GetLogEquipment() *doc.LogEquipment
	GetUser() input.ReqUser
}

type logEquipModel struct {
	mgo MgoDBModel
	log log.Logger
}

func (lem *logEquipModel) Log(led LogEquipDoc) error {
	_, err := lem.mgo.Save(led.GetLogEquipment(), led.GetUser())
	return err
}
