package model

import (
	"apiscerana/core"
	"apiscerana/dao/doc"
	"apiscerana/mock"
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func Test_Find(t *testing.T) {
	core.InitConfByPath("./testfile")
	ctx := context.Background()
	c, err := core.GetDI().NewMongoDBClient(ctx)
	assert.Nil(t, err)
	l := mock.NewLogger()
	mdbm := NewMgoModel(ctx, c.GetCoreDB(), l)
	con := &doc.Controller{}
	r, err := mdbm.Find(con, bson.M{"boxmd5": "7038dbfe9d6ed5654229abd8126bd9d6"})
	cary := r.([]*doc.Controller)
	for _, a := range cary {
		fmt.Println(a.ID)
		fmt.Println(a.GatewayID())
		gw := &doc.Gateway{ID: a.GatewayID()}
		err = mdbm.FindByID(gw)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(gw)
		break
	}
	assert.True(t, false)
}
