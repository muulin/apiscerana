package model

import (
	"apiscerana/api/input"
	"apiscerana/core"
	"apiscerana/core/api"
	"apiscerana/core/dao"
	"apiscerana/dao/doc"
	"apiscerana/mock"
	"errors"
	"net/http"

	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Test_loadFromFile(t *testing.T) {
	core.SetDI(mock.NewMockDI(
		map[string]interface{}{
			"GetConfigFile": func(fn string) (string, error) {
				return "./testfile/" + fn, nil
			},
		},
	))

	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAppModel(mockMgo, l)
	// test Lang Update
	v := m.CheckAppLangVers("en", "11.1")
	assert.Equal(t, `https://beta2-api.dforcepro.com/lang/en@16.0.txt`, v.Url)
	assert.Equal(t, "16.0", v.Version)
}

func Test_CheckAppLangVers(t *testing.T) {
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				a := d.(*doc.App)
				a.ID = primitive.NewObjectID()
				a.Lang = map[string]doc.Version{
					"EN": doc.Version{
						Url:     "enlangurl",
						Version: "12.0",
					},
				}
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAppModel(mockMgo, l)

	// test Lang Update
	v := m.CheckAppLangVers("EN", "11.1")
	assert.Equal(t, "enlangurl", v.Url)
	assert.Equal(t, "12.0", v.Version)

	v = m.CheckAppLangVers("EN", "13.0")
	assert.Equal(t, "enlangurl", v.Url)
	assert.Equal(t, "12.0", v.Version)

	// test Lang not exist
	v = m.CheckAppLangVers("TW", "11.1")
	assert.Nil(t, v)

	// test Lang equil
	v = m.CheckAppLangVers("EN", "12.0")
	assert.Nil(t, v)
}

func Test_CheckDeskLangVers(t *testing.T) {
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				a := d.(*doc.App)
				a.ID = primitive.NewObjectID()
				a.DeskLang = map[string]doc.Version{
					"EN": doc.Version{
						Url:     "enlangurl",
						Version: "12.0",
					},
				}
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAppModel(mockMgo, l)

	// test Lang Update
	v := m.CheckDeskLangVers("EN", "11.1")
	assert.Equal(t, "enlangurl", v.Url)
	assert.Equal(t, "12.0", v.Version)

	v = m.CheckDeskLangVers("EN", "13.0")
	assert.Equal(t, "enlangurl", v.Url)
	assert.Equal(t, "12.0", v.Version)

	// test Lang not exist
	v = m.CheckDeskLangVers("TW", "11.1")
	assert.Nil(t, v)

	// test Lang equil
	v = m.CheckDeskLangVers("EN", "12.0")
	assert.Nil(t, v)
}

func Test_CheckAppVers(t *testing.T) {
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				a := d.(*doc.App)
				a.ID = primitive.NewObjectID()
				a.IOS.Url = "iosurl"
				a.IOS.Version = "1.2.3"
				a.Android.Url = "androidurl"
				a.Android.Version = "1.1.1"
				a.Desk.Url = "deskurl"
				a.Desk.Version = "1.0.12"
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAppModel(mockMgo, l)

	// test get new version info
	v := m.CheckAppVers(doc.PlatformIOS, "1.2.2")
	assert.Equal(t, "iosurl", v.Url)
	assert.Equal(t, "1.2.3", v.Version)

	// test ios greater version
	v = m.CheckAppVers(doc.PlatformIOS, "1.2.4")
	assert.Nil(t, v)
	v = m.CheckAppVers(doc.PlatformIOS, "1.2.3")
	assert.Nil(t, v)

	// test android new version info
	v = m.CheckAppVers(doc.PlatformAndroid, "1.0.1")
	assert.Equal(t, "androidurl", v.Url)
	assert.Equal(t, "1.1.1", v.Version)

	// test desk new version info
	v = m.CheckAppVers(doc.PlatformDesk, "1.0.1")
	assert.Equal(t, "deskurl", v.Url)
	assert.Equal(t, "1.0.12", v.Version)
}
func Test_IsAppReview(t *testing.T) {
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				a := d.(*doc.App)
				a.ID = primitive.NewObjectID()
				a.IOSReviw = "1.2.2"
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAppModel(mockMgo, l)

	// testing IOS review version
	r := m.IsAppReview(doc.PlatformIOS, "1.2.2")
	assert.True(t, r)

	// testing IOS not review version
	r = m.IsAppReview(doc.PlatformIOS, "1.1.2")
	assert.False(t, r)

	// testing Platform Android
	r = m.IsAppReview(doc.PlatformAndroid, "1.2.2")
	assert.False(t, r)

	// testing not in list platform
	r = m.IsAppReview("aaaaaa", "1.2.2")
	assert.False(t, r)
}

func Test_AuthLogin(t *testing.T) {
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				u := d.(*doc.User)
				u.AddSys("test", "user")
				u.Enable = true
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAuthModel(mockMgo, nil, l)
	i := input.LoginV2{
		Account:  "test@gmail.com",
		Pwd:      "test",
		PhoneID:  "phoneID",
		Platform: doc.PlatformAndroid,
		Push:     "push",
		SysCode:  "test",
	}
	// testing login success
	u, err := m.Login(i)
	assert.Nil(t, err)
	assert.Equal(t, u.Group, "user")

	// testing user not found
	mockMgo.SetMockFunc("FindOne", func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
		return errors.New("not found")
	})
	m = NewAuthModel(mockMgo, nil, l)
	u, err = m.Login(i)
	assert.Nil(t, u)
	apiErr, ok := err.(api.ApiError)
	assert.True(t, ok)
	assert.Equal(t, http.StatusNotFound, apiErr.GetStatus())

	// testing user disable
	mockMgo.SetMockFunc("FindOne", func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
		u := d.(*doc.User)
		u.AddSys("test", "user")
		u.Enable = false
		return nil
	})
	m = NewAuthModel(mockMgo, nil, l)
	u, err = m.Login(i)
	assert.Nil(t, u)
	apiErr, ok = err.(api.ApiError)
	assert.True(t, ok)
	assert.Equal(t, http.StatusForbidden, apiErr.GetStatus())
}

func Test_AuthLogout(t *testing.T) {
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"FindOne": func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
				return nil
			},
		},
	)
	l := mock.NewLogger()
	m := NewAuthModel(mockMgo, nil, l)
	u := input.NewReqUser("test", "testname", "testpid", "user")
	err := m.Logout(u, "testToken")
	assert.Nil(t, err)

	mockMgo.SetMockFunc("FindOne", func(d dao.DocInter, q bson.M, opts ...*options.FindOneOptions) error {
		upt := d.(*doc.UserPushToken)
		upt.ID = primitive.NewObjectID()
		return nil
	})
	err = m.Logout(u, "testToken")
	assert.Nil(t, err)

	redis := &mock.MockRedis{
		BasicMock: map[string]interface{}{
			"Del": func(k string) (int64, error) {
				return 0, nil
			},
		},
	}
	m = NewAuthModel(mockMgo, redis, l)
	err = m.Logout(u, "testToken")
	assert.Nil(t, err)

	redis.SetMockFunc("Del", func(k string) (int64, error) {
		return 0, errors.New("redis error")
	})
	err = m.Logout(u, "testToken")
	assert.NotNil(t, err)
}
