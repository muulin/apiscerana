package model

import (
	"apiscerana/core/db"
	"apiscerana/core/log"
	"apiscerana/dao/doc"
	"apiscerana/util"
	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func NewUserPermModel(mdbm MgoDBModel, l log.Logger) *userPermModel {
	m := &userPermModel{
		mdbm: mdbm,
		log:  l,
	}
	return m
}

func NewUserPermModelByReq(req *http.Request) *userPermModel {
	l := util.GetCtxVal(req, log.CtxLogKey)

	m := &userPermModel{
		mdbm: NewMgoModelByReq(req, db.CoreDB),
		log:  l.(log.Logger),
	}
	return m
}

type userPermModel struct {
	mdbm MgoDBModel
	log  log.Logger
}

func (up *userPermModel) GetUserEquipIDs(acc string) []primitive.ObjectID {
	r, err := up.mdbm.Find(&doc.UserPermission{}, bson.M{"account": acc})
	if err != nil {
		up.log.Info(err.Error())
		return nil
	}
	upl, ok := r.([]*doc.UserPermission)
	if !ok {
		return nil
	}
	var oids []primitive.ObjectID
	for _, u := range upl {
		if u.EquipID != nil {
			oids = append(oids, *u.EquipID)
		}
	}
	return oids
}

func (up *userPermModel) GetUserEquipPermMap(acc string) map[string]string {
	r, err := up.mdbm.Find(&doc.UserPermission{}, bson.M{"account": acc})
	if err != nil {
		up.log.Info(err.Error())
		return nil
	}
	upl, ok := r.([]*doc.UserPermission)
	if !ok {
		return nil
	}
	result := make(map[string]string)
	for _, u := range upl {
		if u.EquipID != nil {
			result[u.EquipID.Hex()] = u.Permission
		}
	}
	return result
}
