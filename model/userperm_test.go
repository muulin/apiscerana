package model

import (
	"apiscerana/core/dao"
	"apiscerana/dao/doc"
	"apiscerana/mock"
	"errors"

	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func Test_GetUserEquipPermMap(t *testing.T) {
	id1, _ := primitive.ObjectIDFromHex("a123456789b123456789ceaa")
	id2, _ := primitive.ObjectIDFromHex("b123456789c123456789deaa")
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"Find": func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
				return []*doc.UserPermission{
					{
						EquipID:    &id1,
						Permission: "own",
					},
					{
						EquipID:    &id2,
						Permission: "user",
					},
				}, nil
			},
		},
	)
	l := mock.NewLogger()
	upm := NewUserPermModel(mockMgo, l)

	// test get user equip perm map
	m := upm.GetUserEquipPermMap("test")
	p, ok := m[id1.Hex()]
	assert.Equal(t, "own", p)
	assert.True(t, ok)

	// test return nil
	mockMgo.SetMockFunc("Find", func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
		return nil, nil
	})
	m = upm.GetUserEquipPermMap("test")
	assert.Nil(t, m)

	// test return error
	mockMgo.SetMockFunc("Find", func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
		return nil, errors.New("not found")
	})
	m = upm.GetUserEquipPermMap("test")
	assert.Nil(t, m)

}

func Test_GetUserEquipIDs(t *testing.T) {
	id1, _ := primitive.ObjectIDFromHex("a123456789b123456789ceaa")
	id2, _ := primitive.ObjectIDFromHex("b123456789c123456789deaa")
	mockMgo := mock.NewMockMongo(
		map[string]interface{}{
			"Find": func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
				return []*doc.UserPermission{
					{
						EquipID:    &id1,
						Permission: "own",
					},
					{
						EquipID:    &id2,
						Permission: "user",
					},
				}, nil
			},
		},
	)
	l := mock.NewLogger()
	upm := NewUserPermModel(mockMgo, l)

	// test get user equip perm map
	m := upm.GetUserEquipIDs("test")
	assert.Equal(t, 2, len(m))
	assert.Equal(t, id1, m[0])
	assert.Equal(t, id2, m[1])

	// test return nil
	mockMgo.SetMockFunc("Find", func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
		return nil, nil
	})
	m = upm.GetUserEquipIDs("test")
	assert.Nil(t, m)

	// test return error
	mockMgo.SetMockFunc("Find", func(d dao.DocInter, q bson.M, opts ...*options.FindOptions) (interface{}, error) {
		return nil, errors.New("not found")
	})
	m = upm.GetUserEquipIDs("test")
	assert.Nil(t, m)

}
